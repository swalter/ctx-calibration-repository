#!/bin/bash
# Example script for the CTX processing pipeline with application of a specific flat-field file. Run it with the image name as the parameter, e.g.:
# ./proc_ctx.sh B01_009838_2108_XI_30N319W.IMG
# This script is just an example and has not been thoroughly tested. ISIS environment has to be loaded.

image=`basename $1 .IMG`
calfile=flats/ctxFlat_0003.cub
if [ ! -f ${calfile} ] ; then
  echo "Cal file ${calfile} not found! I will quit"
  exit -1
fi
mroctx2isis from=$1 to=${image}.cub
spiceinit from=${image}.cub
declare -i spatialsumming
spatialsumming=`catlab from=${image}.input.cub|grep SpatialSumming|awk -F= '{print $2}'`
ctxcal from=${image}.cub to=${image}.cal.cub flatfile=${calfile}
if [ "$spatialsumming" -eq 1 ] ; then
  ctxevenodd from=${image}.cal.cub to=${image}.eo.cub
  calimg=${image}.eo.cub
else
  calimg=${image}.cal.cub
fi
cam2map from=${calimg} to=${image}.map.cub
exit 0

#!/bin/bash
# This script has to be called on every single CTX image, e.g.:
# ./1_pre-process.sh B01_009838_2108_XI_30N319W.IMG
image=`basename $1 .IMG`
mroctx2isis from=$1 to=${image}.input.cub
spiceinit from=${image}.input.cub
ctxcal from=${image}.input.cub to=${image}.cal.cub flatfile=ctx_noflat.cub

#!/bin/bash
# After creating a text file containing all the images to be included for the flatfield calculation, the script is submitted with two parameters (first the name of the list of pre-processed images, then the name of the output file name), e.g.:
# ./2_makeflat.sh ctxFlat_0003.lis ctxFlat_0003
stdev=0.1
numl=100
name=$2
makeflat fromlist=$1 to=${name}.cub exclude=${name}.exclude.txt stdevtol=${stdev} numlines=${numl} -verbose -log=${name}_makeflat.log

The folder "flats" contains the actual flat field image files in ISIS3 cub format.
The bash shell script file "proc_ctx.sh" is an example of how the calibration files can be used to create calibrated and map-projected images.

The "supplement" folder contains the detailed input lists of the images for each flat field file created, as well as the log and output files for the flat-field processing.
Two minimal example scripts are provided to show exactly how the images were preprocessed and the flatfield files were created.

The "validation" folder contains two sets of preview images for visual validation. The folder "subset-1000" contains 1000 images randomly chosen from the full mission timespan. The folder "subset-200_2019-to-march2020" contains 200 images from the time period between 2019/01/01 to 2020/03/31.
The validation images always show the images created with the previews calibration (version 0002) on the left, and the new calibration (version 0003) on the right.

Authors: Sebastian H.G. Walter, Klaus-Michael Aye, Ralf Jaumann, Frank Postberg. DOI of the repository: https://dx.doi.org/10.17169/refubium-41645

This work is licensed under the Creative Commons Attribution 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
